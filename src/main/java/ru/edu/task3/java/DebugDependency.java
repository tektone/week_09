package ru.edu.task3.java;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
public class DebugDependency implements DependencyObject {
    @Override
    public String getValue() {
        return "DEBUG";
    }
}
