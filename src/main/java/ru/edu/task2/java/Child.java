package ru.edu.task2.java;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
public class Child {

    private TimeKeeper timeKeeper;

    public TimeKeeper getTimeKeeper() {
        return timeKeeper;
    }
}
