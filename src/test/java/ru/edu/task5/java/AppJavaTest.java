package ru.edu.task5.java;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import ru.edu.task5.Task5Common;

import static org.junit.Assert.*;

/**
 * ReadOnly
 */
public class AppJavaTest extends Task5Common {

    @Test
    public void run() {
        ApplicationContext ctx = AppJava.run();
        run(ctx);
    }
}